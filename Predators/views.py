from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from Predators.models import Predator


def hello(request):
    return HttpResponse(
        '<html><head><title>Predators digest</title></head>'
        '<body>'
        '  <h1>Hello and welcome to my Predators digest</h1>'
        '<a href="predators/">Click Here!</a>'
        '</body>'
        '</html>'
    )


def home(request):
    return HttpResponse(
        '<html><head><title>Predators</title></head>'
        '<body>'
        '  <h1>Ok, here is top 3 predators on the Earth</h1>'
        '<a href="bear/">1.Bear!</a><br>'
        '<a href="shark/">2.Tiger Shark!</a><br>'
        '<a href="crocodile/">3.Crocodile!</a><br>'
        '</body>'
        '</html>'
    )


def bear(request):
    coll = Predator.objects.all()
    descr = 'none'
    for i in coll:
        if i.name == u'bear':
            descr = i.description

    return HttpResponse('<html><head><title>Predators</title></head>'
                        '<body>'
                        '  <h1>%s</h1>'
                        '</body>'
                        '</html>' % descr)


def shark(request):
    coll = Predator.objects.all()
    descr = 'none'
    for i in coll:
        if i.name == u'shark':
            descr = i.description

    return HttpResponse('<html><head><title>Predators</title></head>'
                        '<body>'
                        '  <h1>%s</h1>'
                        '</body>'
                        '</html>' % descr)


def crocodile(request):
    coll = Predator.objects.all()
    descr = 'none'
    for i in coll:
        if i.name == u'crocodile':
            descr = i.description

    return HttpResponse('<html><head><title>Predators</title></head>'
                        '<body>'
                        '  <h1>%s</h1>'
                        '</body>'
                        '</html>' % descr)