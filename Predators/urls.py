from django.conf.urls import url
from Predators.views import home, hello, bear, shark, crocodile


urlpatterns = [
    url(r'^$', hello),
    url(r'^predators/$', home),
    url(r'^predators/bear/$', bear),
    url(r'^predators/shark/$', shark),
    url(r'^predators/crocodile/$', crocodile)
]